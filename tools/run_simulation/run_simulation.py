#!/usr/bin/env python3

# IMPORTANT! for any changes here the docker image (docker-registry.ebrains.eu/tc/cwl-tools/bucket_fetch_file) has to be updated

import argparse
import shutil
import os, sys
import requests as re
## Import tools:
import matplotlib.pyplot as plt
import numpy as np
import json
import os
from IPython.display import Video
from nuu_parameter_Mouse_512 import Parameter
import nuu_tools_simulation as tools
from plot import multiview, multiview_one,multiview_one_bar, prepare_surface_regions, animation_nuu


def sc3_run_simulation(folder_root, synch_root, bvals, run_sim, cut_transient, simname):

    if not folder_root.startswith('/'):
        folder_root = '/' + folder_root 
    if not synch_root.startswith('/'):
        synch_root = '/' + folder_root 
    parameters = Parameter()

    # define parameters of the simulation
    bvals = [int(i) for i in bvals] 
    run_sim = float(run_sim)
    cut_transient = float(cut_transient)
    Iext = 0.000315 # External input
    print(simname)
    print(simname[0])
    simname = [i for i in simname[0].split(',')]
    print(simname)
    print(type(simname))

    ## Set the parameters of the stimulus (choose stimval = 0 to simulate spontaneous activity)
    stimval = 0 #  stimulus strength
    stimdur = 50 # ms, duration of the stimulus
    stimtime_mean = 2500. # ms, time after simulation start (it will be shufled)
    stim_region = 260 # Right barrel field in the primary somatosensory area of the cortex.

    # set up the simulator
    parameters.parameter_simulation['path_result'] = os.getcwd() + synch_root
    simulator = tools.init(parameters.parameter_simulation,
                          parameters.parameter_model,
                          parameters.parameter_connection_between_region,
                          parameters.parameter_coupling,
                          parameters.parameter_integrator,
                          parameters.parameter_monitor)

    # run the simulation
    for simnum in range(len(bvals)):
        parameters.parameter_model['b_e'] = bvals[simnum] 

        parameters.parameter_model['external_input_ex_ex']=Iext
        parameters.parameter_model['external_input_in_ex']=Iext

        weight = list(np.zeros(simulator.number_of_nodes))
        weight[stim_region] = stimval # region and stimulation strength of the region 0 

        parameters.parameter_stimulus["tau"]= stimdur # stimulus duration [ms]
        parameters.parameter_stimulus["T"]= 1e9 # interstimulus interval [ms]
        parameters.parameter_stimulus["weights"]= weight
        parameters.parameter_stimulus["variables"]=[0] #variable to kick

        print('b_e =',bvals[simnum])

        parameters.parameter_stimulus['onset'] = cut_transient + 0.5*(run_sim-cut_transient)
        stim_time = parameters.parameter_stimulus['onset']
        stim_steps = stim_time*10 #number of steps until stimulus

        parameters.parameter_simulation['path_result'] = os.getcwd() + folder_root +'/result_b_'+str(bvals[simnum])+'/'
        print(parameters.parameter_simulation['path_result'])
        simulator = tools.init(parameters.parameter_simulation,
                              parameters.parameter_model,
                              parameters.parameter_connection_between_region,
                              parameters.parameter_coupling,
                              parameters.parameter_integrator,
                              parameters.parameter_monitor,
                              parameter_stimulation=parameters.parameter_stimulus)
        if stimval:
            print ('    Stimulating for {1} ms, {2} nS in the {0}\n'.format(simulator.connectivity.region_labels[stim_region],parameters.parameter_stimulus['tau'],stimval))

        tools.run_simulation(simulator,
                    run_sim,                            
                    parameters.parameter_simulation,
                    parameters.parameter_monitor)



parser = argparse.ArgumentParser()
parser.add_argument('folder_root', help='input folder')
parser.add_argument('synch_root', help='synch_root')
parser.add_argument('bvals', type=int, nargs="+", help='b (adaptation) values')
parser.add_argument('run_sim', help='len simulation')
parser.add_argument('cut_transient', help='cut transient ')
parser.add_argument('simname', type=str, nargs="+", help='names for labelling plots')
args = parser.parse_args()

# run
sc3_run_simulation(args.folder_root, args.synch_root, args.bvals, args.run_sim, args.cut_transient, args.simname)
