#!/usr/bin/env python3

# IMPORTANT! for any changes here the docker image (docker-registry.ebrains.eu/tc/cwl-tools/bucket_push_file) has to be updated

import argparse
import shutil
import os
import requests as re

# function that pushes an object to a Collab bucket
def bucket_push_file(bucket_id, folder, token):
    files = os.listdir(folder)
    # get an upload url
    DATA_PROXY_ENDPOINT = 'https://data-proxy.ebrains.eu/api/v1/buckets'
    AUTHORIZATION_HEADERS = {'Authorization': f'Bearer {token}'}
    for file in files: 
        r_url = re.put(f"{DATA_PROXY_ENDPOINT}/{bucket_id}/{file}", headers=AUTHORIZATION_HEADERS)   # temp url
        url = r_url.json()['url']
        # print bucket stats before upload
        try: 
            print('1')
            response = re.put(url, data=open(folder+'/'+file, 'rb').read())
        except:
            print('2')
            response = re.put(url, data=open(file, 'rb').read())

parser = argparse.ArgumentParser()
parser.add_argument('bucket_id', help='bucket where the file will be uploaded')
parser.add_argument('folder', help='object/file to be uploaded')
parser.add_argument('token', help='token for access to the data-proxy')
args = parser.parse_args()

# push input_file to bucket
bucket_push_file(args.bucket_id, args.folder, args.token)
