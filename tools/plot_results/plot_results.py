#!/usr/bin/env python3

# IMPORTANT! for any changes here the docker image (docker-registry.ebrains.eu/tc/cwl-tools/bucket_fetch_file) has to be updated

import argparse
import shutil
import os
import requests as re
from nuu_parameter_Mouse_512 import Parameter
import nuu_tools_simulation as tools
from plot import multiview, multiview_one,multiview_one_bar, prepare_surface_regions, animation_nuu
## Import tools:
import matplotlib.pyplot as plt
import numpy as np
import json
import os
from IPython.display import Video
from datetime import datetime


def sc3_plot_results(results_folder, output_dir_name, bvals, run_sim, cut_transient, simname):
    if os.path.exists(output_dir_name):
        shutil.rmtree(output_dir_name)
    os.makedirs(output_dir_name)


    # load results
    FR_exc = []
    FR_inh = []
    Ad_exc = []
    run_sim = float(run_sim)
    cut_transient = float(cut_transient)
    simname = [i for i in simname[0].split(',')]
    for simnum in range(len(bvals)):
        '''load result'''
        sim_name = 'result_b_'+str(bvals[simnum])+'/'
        print ('... loading file: ' + sim_name)       
        file_name =  results_folder + '/' + sim_name
        result = tools.get_result(file_name,cut_transient,run_sim)
                
        '''fill variables'''
        time_s = result[0][0]*1e-3 #from ms to sec
        FR_exc.append(result[0][1][:,0,:]*1e3) # from KHz to Hz; Excitatory firing rate
        FR_inh.append(result[0][1][:,1,:]*1e3) # from KHz to Hz; Inhibitory firing rate
        Ad_exc.append(result[0][1][:,5,:]) # nA; Excitatory adaptation
    
    # create and save figures
    plot_traces(bvals, time_s, FR_exc, FR_inh, Ad_exc, simname, output_dir_name)
    plot_spectral_densities(time_s, simname, bvals, FR_exc, FR_inh, output_dir_name)
    


def plot_traces(bvals, time_s, FR_exc, FR_inh, Ad_exc, simname, output_dir_name):
    # figure traces visualize
    fig, axes = plt.subplots(nrows=2,ncols=len(bvals),figsize=(16,8))
    plt.rcParams.update({'font.size': 14})

    if len(bvals)>1:
        for simnum in range(len(bvals)):

            '''plot traces'''
            Li = axes[0,simnum].plot(time_s,FR_inh[simnum],color='darkred') # [times, regions]
            Le = axes[0,simnum].plot(time_s,FR_exc[simnum],color='SteelBlue') # [times, regions]

            axes[1,simnum].plot(time_s,Ad_exc[simnum][:,0:10],color='goldenrod') # [times, regions]

            axes[0,simnum].set_xlabel('Time (s)')
            axes[1,simnum].set_xlabel('Time (s)')

            axes[0,simnum].set_ylabel('Firing rate (Hz)')
            axes[1,simnum].set_ylabel('Adaptation (nA)')
            
            axes[0,simnum].set_title('b = {0} pA\n({1})'.format(bvals[simnum],simname[simnum]))
            axes[0,simnum].set_ylim([-15,55])
            axes[0,simnum].legend([Li[0], Le[0]], ['Inh.','Exc.'], loc='best')
            
            for ax in axes.reshape(-1):
                ax.set_xlim([1,3])
                ax.set_xticks([1,1.5,2,2.5,3])
    else:
        '''plot traces'''
        Li = axes[0].plot(time_s,FR_inh[0],color='darkred') # [times, regions]
        Le = axes[0].plot(time_s,FR_exc[0],color='SteelBlue') # [times, regions]

        axes[1].plot(time_s,Ad_exc[0][:,0:10],color='goldenrod') # [times, regions]

        axes[0].set_xlabel('Time (s)')
        axes[1].set_xlabel('Time (s)')

        axes[0].set_ylabel('Firing rate (Hz)')
        axes[1].set_ylabel('Adaptation (nA)')
        
        axes[0].set_title('b = {0} pA\n({1})'.format(bvals[0],simname[0]))
        axes[0].set_ylim([-15,55])
        axes[0].legend([Li[0], Le[0]], ['Inh.','Exc.'], loc='best')
        
        for ax in axes.reshape(-1):
            ax.set_xlim([1,3])
            ax.set_xticks([1,1.5,2,2.5,3])
    plt.tight_layout()
    timestamp = int(datetime.now().timestamp()) # timestamp to avoid overwriting files in bucket
    plt.savefig(f'{output_dir_name}/fig_traces_{timestamp}.png')

def plot_spectral_densities(time_s, simname, bvals, FR_exc, FR_inh, output_dir_name):
    """
    Function to plot the spectral densities 
    Proper documentation of this function is needed
    """    
    fig, axes = plt.subplots(nrows=1,ncols=len(bvals),figsize=(12,4))
    if not isinstance(axes, np.ndarray):
        axes = np.array([axes])
    plt.rcParams.update({'font.size': 14})

    f_sampling = 1.*len(time_s)/time_s[-1] # time in seconds, f_sampling in Hz
    frq = np.fft.fftfreq(len(time_s), 1/f_sampling)

    pwr_region_E = []
    pwr_region_I = []

    for simnum in range(len(bvals)):
        Esig = np.transpose(FR_exc[simnum])
        Isig = np.transpose(FR_inh[simnum])
        nnodes = len(Esig)
        for e_reg in range(nnodes):
            pwr_region_E.append(np.abs(np.fft.fft(Esig[e_reg]))**2)
            pwr_region_I.append(np.abs(np.fft.fft(Isig[e_reg]))**2)

        mean_E_Hz = np.mean(pwr_region_E, axis=0)
        mean_I_Hz = np.mean(pwr_region_I, axis=0)
        std_e = np.std(pwr_region_E, axis=0) #std fft between regions
        std_i = np.std(pwr_region_I, axis=0) #std fft between regions

        high_e = mean_E_Hz[frq > 0]+std_e[frq > 0]/np.sqrt(nnodes)
        high_i = mean_I_Hz[frq > 0]+std_i[frq > 0]/np.sqrt(nnodes)

        low_e =  mean_E_Hz[frq > 0]-std_e[frq > 0]/np.sqrt(nnodes)
        low_i =  mean_I_Hz[frq > 0]-std_i[frq > 0]/np.sqrt(nnodes)

        # plot psds
        axes[simnum].loglog(frq[frq > 0], mean_I_Hz[frq > 0], 'darkred', alpha=0.9, label='Inh.')
        axes[simnum].loglog(frq[frq > 0], mean_E_Hz[frq > 0], color='SteelBlue', label='Exc.')
        axes[simnum].fill_between(frq[frq > 0], high_i, low_i, color = 'DarkSalmon', alpha = 0.4)
        axes[simnum].fill_between(frq[frq > 0], high_e, low_e, color = 'DodgerBlue', alpha = 0.4)
        
        axes[simnum].set_xlabel('Frequency (Hz)')
        axes[simnum].set_ylabel('Power')  
        for ax in axes.reshape(-1):
            ax.legend()
            ax.set_ylim(10e-4,10e10)
        
        axes[simnum].set_title(f'b = {bvals[simnum]} pA\n{simname[simnum]}')

    plt.tight_layout()
    timestamp = int(datetime.now().timestamp()) # timestamp to avoid overwriting files in bucket
    plt.savefig(f'{output_dir_name}/fig_psd_{timestamp}.png')





# main    
parser = argparse.ArgumentParser()
parser.add_argument('results_folder', help='input folder')
parser.add_argument('output_dir_name', help='output dir name')
parser.add_argument('bvals', type=int, nargs="+", help='b (adaptation) values')
parser.add_argument('run_sim', help='len simulation')
parser.add_argument('cut_transient', help='cut transient ')
parser.add_argument('simname', type=str, nargs="+", help='names for labelling plots')
args = parser.parse_args()
# run
sc3_plot_results(args.results_folder, args.output_dir_name, args.bvals, args.run_sim, args.cut_transient, args.simname)
