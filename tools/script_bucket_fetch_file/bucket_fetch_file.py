#!/usr/bin/env python3

# IMPORTANT! for any changes here the docker image (docker-registry.ebrains.eu/tc/cwl-tools/bucket_fetch_file) has to be updated

import argparse
import shutil
import os
import requests as re

# function that fetches an object from a Collab bucket
def bucket_fetch_file(bucket_id, files_list, token):
    with open(files_list) as f:
        object_names = f.readlines()

    object_names = [i.split('\n')[0] for i in object_names]
    # loop over selected files
    for object_name in object_names:
        # get a download url
        DATA_PROXY_ENDPOINT = 'https://data-proxy.ebrains.eu/api/v1/buckets'
        AUTHORIZATION_HEADERS = {'Authorization': f'Bearer {token}'}
        r_url = re.get(f'{DATA_PROXY_ENDPOINT}/{bucket_id}/{object_name}?redirect=false', headers=AUTHORIZATION_HEADERS)  # temp url
        url = r_url.json()['url']
        # get file using download url
        r = re.get(url, stream=True)
        if r.status_code == 200:
            #os.makedirs(os.path.dirname(object_name), exist_ok=True)
            with open(object_name, 'wb') as f:
                r.raw.decode_content = True
                shutil.copyfileobj(r.raw, f)
                print(f'File {object_name} is now in the storage space.')

# parser = argparse.ArgumentParser()
# parser.add_argument('bucket_id', help='bucket containing the input file')
# parser.add_argument('input_file', help='input file to download')
# parser.add_argument('token', help='token for access to the data-proxy')
# args = parser.parse_args()
# fetch input_file from bucket


# bucket_fetch_file(args.bucket_id, args.input_file, args.token)
bucket_fetch_file('showcase3-cwl-integration', 'files_to_fetch.txt', 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJfNkZVSHFaSDNIRmVhS0pEZDhXcUx6LWFlZ3kzYXFodVNJZ1RXaTA1U2k0In0.eyJleHAiOjE2NzAzMTg2MDQsImlhdCI6MTY2OTcxMzgwNCwiYXV0aF90aW1lIjoxNjY5MzczNzkzLCJqdGkiOiJlODVlYjBlOC05M2Q0LTRlNzEtYjk3MS1kMjlmNjNmNmQxY2MiLCJpc3MiOiJodHRwczovL2lhbS5lYnJhaW5zLmV1L2F1dGgvcmVhbG1zL2hicCIsImF1ZCI6WyJpbWdfc3ZjIiwidHV0b3JpYWxPaWRjQXBpIiwieHdpa2kiLCJqdXB5dGVyaHViLWpzYyIsInRlYW0iLCJwbHVzIiwiZ3JvdXAiXSwic3ViIjoiMThhZGVlMTMtODhmYy00YjIzLTk3YTAtYzRhZDMyNzRlNWM2IiwidHlwIjoiQmVhcmVyIiwiYXpwIjoianVweXRlcmh1YiIsInNlc3Npb25fc3RhdGUiOiIzZWI4MTM0YS0wNGM5LTQ3ODMtODI2Yi1mZTFlYTI4YzkwYTYiLCJhY3IiOiIwIiwiYWxsb3dlZC1vcmlnaW5zIjpbImh0dHBzOi8vanVweXRlcmh1Yi5hcHBzLmpzYy5oYnAuZXUvIiwiaHR0cHM6Ly9sYWIuZWJyYWlucy5ldS8iLCJodHRwczovL2xhYi5qc2MuZWJyYWlucy5ldS8iXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIl19LCJzY29wZSI6ImNvbGxhYi5kcml2ZSBwcm9maWxlIG9mZmxpbmVfYWNjZXNzIGNsYi53aWtpLndyaXRlIGdyb3VwIGNsYi53aWtpLnJlYWQgdGVhbSBjbGIuZHJpdmU6d3JpdGUgcXVvdGEgZW1haWwgcm9sZXMgb3BlbmlkIGNsYi5kcml2ZTpyZWFkIiwic2lkIjoiM2ViODEzNGEtMDRjOS00NzgzLTgyNmItZmUxZWEyOGM5MGE2IiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImdlbmRlciI6Im51bGwiLCJuYW1lIjoiQXJuYXUgTWFuYXNhbmNoIiwibWl0cmVpZC1zdWIiOiIzMDgxNTEiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJhcm5hdW1hbmFzYW5jaCIsImdpdmVuX25hbWUiOiJBcm5hdSIsImZhbWlseV9uYW1lIjoiTWFuYXNhbmNoIiwiZW1haWwiOiJtYW5hc2FuY2hAcmVjZXJjYS5jbGluaWMuY2F0In0.vEtGBOlQePa4VRUymHMsUOeNA63H77EnDW6H5pU9LIPUHCtePdSNVlxYOOKaZrrxkOjtRaw0CABA5OCWIZwkwP4wWk2133oRB-zSAuHWApdYK2fh1S9w2SJYvQ64Yer3w1tCxADs4gK9GNfInjrq3PfEBY6n_RCpU7X_oI1OS1qzml4bok7ipSRNH6ARRdDr6a8ptAaCWq6703y2p2IEk_PWWRBzs_NI_JDfBvCLfQ36kKyWaDr9xgRBBtjaTMn7dZytW0heAPQPgCHxATwk-MnVHQ_kyP1wLOXDWS52PtgHMFuFAyoLZsIwb4QdSnKkZ7jW5Un_25XCmCPXCDmeXw')


# python3 bucket_fetch_file.py showcase3-cwl-integration files_to_fetch.txt eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJfNkZVSHFaSDNIRmVhS0pEZDhXcUx6LWFlZ3kzYXFodVNJZ1RXaTA1U2k0In0.eyJleHAiOjE2NjgxNTQ5OTQsImlhdCI6MTY2NzU1MDE5NCwiYXV0aF90aW1lIjoxNjY3Mzc1NjEyLCJqdGkiOiIxNzBkMDkwZi0zZjk0LTRmMGUtYjg0Mi02YmI2ODgzNDA0NzkiLCJpc3MiOiJodHRwczovL2lhbS5lYnJhaW5zLmV1L2F1dGgvcmVhbG1zL2hicCIsImF1ZCI6WyJpbWdfc3ZjIiwidHV0b3JpYWxPaWRjQXBpIiwieHdpa2kiLCJqdXB5dGVyaHViLWpzYyIsInRlYW0iLCJwbHVzIiwiZ3JvdXAiXSwic3ViIjoiMThhZGVlMTMtODhmYy00YjIzLTk3YTAtYzRhZDMyNzRlNWM2IiwidHlwIjoiQmVhcmVyIiwiYXpwIjoianVweXRlcmh1YiIsInNlc3Npb25fc3RhdGUiOiI2NDI0NzNmOC03NWVkLTRhYTMtOTNlYS1hNWE0MDg4NTUxZjEiLCJhY3IiOiIwIiwiYWxsb3dlZC1vcmlnaW5zIjpbImh0dHBzOi8vanVweXRlcmh1Yi5hcHBzLmpzYy5oYnAuZXUvIiwiaHR0cHM6Ly9sYWIuZWJyYWlucy5ldS8iLCJodHRwczovL2xhYi5qc2MuZWJyYWlucy5ldS8iXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIl19LCJzY29wZSI6ImNvbGxhYi5kcml2ZSBwcm9maWxlIG9mZmxpbmVfYWNjZXNzIGNsYi53aWtpLndyaXRlIGdyb3VwIGNsYi53aWtpLnJlYWQgdGVhbSBjbGIuZHJpdmU6d3JpdGUgcXVvdGEgZW1haWwgcm9sZXMgb3BlbmlkIGNsYi5kcml2ZTpyZWFkIiwic2lkIjoiNjQyNDczZjgtNzVlZC00YWEzLTkzZWEtYTVhNDA4ODU1MWYxIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImdlbmRlciI6Im51bGwiLCJuYW1lIjoiQXJuYXUgTWFuYXNhbmNoIiwibWl0cmVpZC1zdWIiOiIzMDgxNTEiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJhcm5hdW1hbmFzYW5jaCIsImdpdmVuX25hbWUiOiJBcm5hdSIsImZhbWlseV9uYW1lIjoiTWFuYXNhbmNoIiwiZW1haWwiOiJtYW5hc2FuY2hAcmVjZXJjYS5jbGluaWMuY2F0In0.v3KO6GSL1mWqNzdLlLVSSiITuqojf7i59JPEkqVUHBvAY69eZToCuCWRHhsZORQLqrFMgAew2rN7wG18Y0nPjA7WV6ZbogbiOcQz-_pNBwSgD4KC01oN7zxG_yfBjJ5WN-1qxd6bJzDO4GGcmDB8NrtuCrQIIe8-Mel52jLsg0XXq-Z-E_lD52Eh2twBNJrO9yJ5ajGXBcb7XG3cHX29994JE7lPR1q1h_F9ow9p9-a5Mg4Ri0_GlL6LHd8UuiZyAmjUOk-votLFuAE2a99dmIIUXRMKHP0jGiMvA6txcjIr87VSFDBU1PP46QBAmeuGoJ7dXGveafn6pj4IiKsitA
