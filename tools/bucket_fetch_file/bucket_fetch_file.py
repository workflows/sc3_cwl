#!/usr/bin/env python3

# IMPORTANT! for any changes here the docker image (docker-registry.ebrains.eu/tc/cwl-tools/bucket_fetch_file) has to be updated

import argparse
import shutil
import os
import requests as re

# function that fetches an object from a Collab bucket
def bucket_fetch_file(bucket_id, files_list, token):
    object_names = [i for i in files_list.split(',')]
    # loop over selected files
    for object_name in object_names:
        # get a download url
        DATA_PROXY_ENDPOINT = 'https://data-proxy.ebrains.eu/api/v1/buckets'
        AUTHORIZATION_HEADERS = {'Authorization': f'Bearer {token}'}
        r_url = re.get(f'{DATA_PROXY_ENDPOINT}/{bucket_id}/{object_name}?redirect=false', headers=AUTHORIZATION_HEADERS)  # temp url
        url = r_url.json()['url']
        # get file using download url
        r = re.get(url, stream=True)
        if r.status_code == 200:
            #os.makedirs(os.path.dirname(object_name), exist_ok=True)
            with open(object_name, 'wb') as f:
                r.raw.decode_content = True
                shutil.copyfileobj(r.raw, f)
                print(f'File {object_name} is now in the storage space.')

parser = argparse.ArgumentParser()
parser.add_argument('bucket_id', help='bucket containing the input file')
parser.add_argument('input_file', help='input file to download')
parser.add_argument('token', help='token for access to the data-proxy')
args = parser.parse_args()

# fetch input_file from bucket
bucket_fetch_file(args.bucket_id, args.input_file, args.token)


