#!/usr/bin/env cwltool

cwlVersion: v1.0
class: CommandLineTool
baseCommand: bucket_fetch_file.py
hints:
  DockerRequirement:
    dockerPull: docker-registry.ebrains.eu/sc3/bucket_fetch_file:latest
inputs:
  bucket_id:
    type: string
    inputBinding:
      position: 1
  object_name:
    type: string[]
    inputBinding:
      position: 2
      itemSeparator: ","
  token:
    type: string
    inputBinding:
      position: 3
outputs:
  fetched_file:
    type: File[]
    outputBinding:
      glob: $(inputs.object_name)
