#!/usr/bin/env cwltool

cwlVersion: v1.0
class: Workflow

inputs:
  bucket_id: string
  input_file: string[]
  token: string
  bvals: int[]
  output_name: string

outputs:
  fetched_file:
    type: File[]
    outputSource: bucket_fetch_file/fetched_file
  sim_results:
    type: File[]
    outputSource: run_simulation/sim_results

steps:
  bucket_fetch_file:
    run: ../../tools/bucket_fetch_file/bucket_fetch_file.cwl
    in:
      bucket_id: bucket_id
      object_name: input_file
      token: token
    out: [fetched_file]
    
  run_simulation:
    run: ../../tools/run_simulation/run_simulation.cwl
    in: 
      bvals: bvals
      output_name: output_name
    out: [sim_results]
        
