#!/usr/bin/env cwltool

cwlVersion: v1.0
class: Workflow

inputs:
  folder_root: string
  synch_root: string
  bvals: int[]
  output_dir_name: string
  run_sim: int
  cut_transient: int
  simname: string[]
  token: string
  bucket_id: string

outputs:
  plots:
    type: Directory
    outputSource: visualization/plots

steps:
  simulation:
    run: steps/run_simulation.cwl
    in:
      folder_root: folder_root
      synch_root: synch_root
      bvals: bvals
      run_sim: run_sim
      cut_transient: cut_transient
      simname: simname
    out: [sim_results]

  visualization:
    run: steps/plot_results.cwl
    in:
      results_folder: simulation/sim_results
      output_dir_name: output_dir_name
      bvals: bvals
      run_sim: run_sim
      cut_transient: cut_transient
      simname: simname
    out: [plots]

  push_bucket:
    run: steps/bucket_push_file.cwl
    in:
      folder: visualization/plots
      token: token
      bucket_id: bucket_id
    out: [out]


s:identifier: https://kg.ebrains.eu/api/instances/5a3576f9-1f88-4d76-8d7b-7ab3664d53ae
s:keywords: ["SC3"]
s:author:
  - class: s:Person
    s:identifier: https://orcid.org/0000-0002-8306-0759
    s:name: Arnau Manasanch
s:codeRepository: https://gitlab.ebrains.eu/workflows/sc3_cwl
s:version: "v1.1"
s:dateCreated: "2023-03-29"


$namespaces:
 s: https://schema.org/

$schemas:
 - https://schema.org/version/latest/schemaorg-current-http.rdf
