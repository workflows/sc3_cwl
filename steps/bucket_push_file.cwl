#!/usr/bin/env cwltool

cwlVersion: v1.0
class: CommandLineTool
baseCommand: bucket_push_file.py
label: "Push a folder to an EBRAINS Collaboratory Bucket"
hints:
  DockerRequirement:
    dockerPull: docker-registry.ebrains.eu/sc3/bucket_push_file@sha256:b309e1fcff9bd43bde370c0c346a458db206e03233522ea3ec4dbc0b45050431

inputs:
  bucket_id:
    type: string
    inputBinding:
      position: 1
  folder:
    type: Directory
    inputBinding:
      position: 2
  token:
    type: string
    inputBinding:
      position: 3

stdout: output.txt
outputs:
  out:
    type: stdout


s:identifier: https://kg.ebrains.eu/api/instances/7bc8ba87-7c52-4fb7-b9c5-c8e12345054b
s:keywords: ["data transfer"]
s:author:
  - class: s:Person
    s:identifier: https://orcid.org/0000-0002-8306-0759
    s:name: Arnau Manasanch
  - class: s:Person
    s:identifier: https://kg.ebrains.eu/api/instances/714f39b8-9fd0-46cd-be4f-9112c87cfe3f
    s:name: Eleni Mathioulaki
s:codeRepository: https://gitlab.ebrains.eu/workflows/sc3_cwl
s:version: "v1.1"
s:dateCreated: "2023-03-29"
s:programmingLanguage: Python


$namespaces:
 s: https://schema.org/

$schemas:
 - https://schema.org/version/latest/schemaorg-current-http.rdf
