#!/usr/bin/env cwltool

cwlVersion: v1.0
class: CommandLineTool
baseCommand: plot_results.py
label: "HBP Showcase 3: Plot results"
hints:
  DockerRequirement:
    dockerPull: docker-registry.ebrains.eu/sc3/plot_results@sha256:148c230a9d62cc217e24f0c39c1269829e766e9be926fbd1584914eb3ffd5eee

inputs:
  results_folder:
    type: Directory
    inputBinding:
      position: 1
  output_dir_name:
    type: string
    inputBinding:
      position: 2
  bvals:
    type: int[]
    inputBinding:
      position: 3
  run_sim:
    type: int
    inputBinding:
      position: 4
  cut_transient:
    type: int
    inputBinding:
      position: 5
  simname:
    type: string[]
    inputBinding:
      itemSeparator: ","
      position: 6

outputs:
  plots:
    type: Directory
    outputBinding:
      glob: $(inputs.output_dir_name)


s:identifier: https://kg.ebrains.eu/api/instances/23b9d27a-0b08-4645-867f-d4d88f296edd
s:keywords: ["visualization"]
s:author:
  - class: s:Person
    s:identifier: https://orcid.org/0000-0002-8306-0759
    s:name: Arnau Manasanch
s:codeRepository: https://gitlab.ebrains.eu/workflows/sc3_cwl
s:version: "v1.1"
s:dateCreated: "2023-03-29"
s:programmingLanguage: Python


$namespaces:
 s: https://schema.org/

$schemas:
 - https://schema.org/version/latest/schemaorg-current-http.rdf
