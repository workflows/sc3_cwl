#!/usr/bin/env cwltool

cwlVersion: v1.0
class: CommandLineTool
baseCommand: run_simulation.py
label: "HBP Showcase 3: Run simulation"
hints:
  DockerRequirement:
    dockerPull: docker-registry.ebrains.eu/sc3/run_simulation@sha256:915468bc91013e3d742d97e9bbe00373a1b243a1f70c2560fe29aac94d30fa4d

inputs:
  folder_root:
    type: string
    inputBinding:
      position: 1
  synch_root:
    type: string
    inputBinding:
      position: 2
  bvals:
    type: int[]
    inputBinding:
      position: 3
  run_sim:
    type: int
    inputBinding:
      position: 4
  cut_transient:
    type: int
    inputBinding:
      position: 5
  simname:
    type: string[]
    inputBinding:
      itemSeparator: ","
      position: 6


outputs:
  sim_results:
    type: Directory
    outputBinding:
      glob: $(inputs.folder_root)


s:identifier: https://kg.ebrains.eu/api/instances/26d2f727-b2f1-4221-9d86-a0d4332bc08a
s:keywords: ["simulation"]
s:author:
  - class: s:Person
    s:identifier: https://orcid.org/0000-0002-8306-0759
    s:name: Arnau Manasanch
s:codeRepository: https://gitlab.ebrains.eu/workflows/sc3_cwl
s:version: "v1.1"
s:dateCreated: "2023-03-29"
s:programmingLanguage: Python


$namespaces:
 s: https://schema.org/

$schemas:
 - https://schema.org/version/latest/schemaorg-current-http.rdf
