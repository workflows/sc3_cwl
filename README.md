# **CWL integration of Showcase 3**

## Description
This project is about the integration in CWL of the Mouse TVB Model notebook that can be found at:
https://wiki.ebrains.eu/bin/view/Collabs/public-3species-tvbadex-eitn-fallschool/Drive


This is a CWL [Workflow](workflow.cwl) consisting of [3 steps (CommandLineTools)](steps).

This workflow builds and executes an AdEx mean-field TVB model, plots the results and pushes these results to an EBRAINS Collab Bucket

Pre-defined inputs can be found in the [workflow_info.yaml](workflow_info.yml) file.

## Inputs 
-  **bvals**: adaptation values
-  **simname**: label for each of the **bvals** 
-  **output_dir_name**: name of the output directory where to store the results
-  **run_sim**: total time of the simulation (in ms)
-  **cut_transient**: transient time (in ms) to cut from the simulation
-  **token**: user's token for the data proxy (clb_oauth.get_token())
-  **bucket_id**: Your bucket id. You should have read and write access to that Bucket 
-  **folder_root** and **synch_root**: auxiliar folders, they should not be changed and should be left as 'result' and 'synch' respectively

## Outputs
- **fig_PSD.png**: final plot containing the PSD of the simulated traces for all nodes under given conditions
- **fig_traces.png**: final plot containing the traces of the simulation for all nodes under given conditions

## Steps

### Step 1: [Running Simulation](steps/run_simulation.cwl)

Runs the simulation of the TVB model

### Step 2: [Plotting Results](steps/plot_results.cwl) 

Plots the results from the simulations

### Step 3: [Pushing files to bucket](steps/bucket_push_file.cwl)  

Pushes the figure to the given Collab Bucket


